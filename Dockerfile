FROM alpine
ARG TGVER
ARG TFVER
RUN set -e && \
    apk add --no-cache --virtual .run-deps \
        curl \
        bash \
        gnupg && \
    cd /root && \
    mkdir -p .tgenv .tfenv && \
    curl -sSL https://github.com/cunymatthieu/tgenv/archive/refs/tags/${TGVER}.tar.gz | tar xz --strip 1 -C .tgenv && \
    curl -sSL https://github.com/tfutils/tfenv/archive/refs/tags/${TFVER}.tar.gz | tar xz --strip 1 -C .tfenv && \
    mkdir .tfenv/versions && \
    echo 'trust-tfenv: yes' > ~/.tfenv/use-gpgv && \
    echo -e 'export TGENV_ARCH=$(arch | sed "s/x86_/amd/" | sed "s/aarch/arm/")\nexport TFENV_ARCH=$TGENV_ARCH\nexport PATH="/root/.tgenv/bin:/root/.tfenv/bin:$PATH"' >> /bin/envs
